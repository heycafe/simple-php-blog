# Simple Blog

A simple blog and site generator that uses your Hey.Café profile or café as the content source. Built to be as minimal as it can be.

## Getting started

Download and simply upload the files to any PHP based host. Everything is minimal and should require no dependencies.
All CSS and JS is all inline inside the theme file for easy editing.