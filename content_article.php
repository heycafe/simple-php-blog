<?php

if ($system_config["heycafe_source_id"]!=false){
	$articleid=str_replace("/article/","",$system_uri);
	$body_info_request=fetchurl("https://endpoint.hey.cafe/api/conversation_info?query=".$articleid."");
	if ($body_info_request!=false){
		$body_info=json_decode($body_info_request,true);
		$allowed=false;
		
		if ($body_info["system_api_error"]!=true){
			$conversation=$body_info["response_data"];
			
			if ($conversation["account"]["id"]==$system_config["heycafe_source_id"]){
				$allowed=true;
			}
			if ($conversation["cafe"]["id"]==$system_config["heycafe_source_id"]){
				$allowed=true;
			}
			
			if ($system_config["heycafe_source_tag"]!=false){
				if ($conversation["tag"]["id"]!=$system_config["heycafe_source_tag"]){
					$allowed=false;
				}
			}
			
			if ($allowed==true){
				
				//--Content
				$contents=$conversation["contents"];
				
				//--Check for source MD files
				if ($conversation["attachments"]!=false){
					foreach ($conversation["attachments"] as $attach){
						if ($attach["type"]=="file"){
							if (strpos($attach["file"],".md") !== false){
								$contents=makesafe(fetchurl($attach["file"]));
							}
						}
					}
				}
				
				//--get title and remove header element if same
				$title=generate_title($contents);
				$contents = trim(str_replace("## ".$title." ##","",$contents));
				$meta_title="".$title." - ".$meta_title."";
				
				//--Newline for description tag!
				$contentsdescrp=generate_firstline($contents);
				if ($contentsdescrp!=""){
					$meta_description=$contentsdescrp;
				}
				
				//--Timestamp
				$timeago=generate_timestamp_ago($conversation["date_created"],$body_info["system_api_timestamp"]);
				
				//--Author
				$author=$conversation["account"]["name"];
				
				//--get attachments
				$attachments="";
				$headerimage="";
				
				//--flip over attachments
				if ($conversation["attachments"]!=false){
					foreach ($conversation["attachments"] as $attach){
						//--header image
						if ($attach["primary"]=="1" AND $attach["type"]=="image"){
							$headerimage="<img class='optimizer_img' source='".$attach["file"]."' src='".$system_blank_image."' alt='Preview image for article'>";
							$meta_image=$attach["file"];
						}
						if ($attach["primary"]=="0" AND $attach["type"]=="image"){
							$attachments.="<div style='display:inline;' id='bottom_file_".$attach["name"]."'><a href='".$attach["file"]."' target='_blank'><img class='optimizer_img' source='".$attach["file"]."' src='".$system_blank_image."' alt='Image attachment' style='max-width:calc(50% - 10px);border-radius:5px;margin:5px;margin-bottom:0px;'></a></div>";
						}
					}
				}
				
				//--Generate
				$content_body.="<bubble id='post_".$conversation["id"]."' class='article'><h1>".$title."</h1><div class='meta'>".$timeago." by ".$author.".</div><div class='headerimage'>".$headerimage."</div><div class='content'><div id='format_".$conversation["id"]."'>".$contents."</div></div><div class='attachements'>".$attachments."</div></bubble>";
				$content_body.="<action type='format_complex' element='format_".$conversation["id"]."'></action>";
				
				$content_body.="<bubble>";
				$content_body.="<script>function opencommentwindow(conversation){ window.location.href='https://beta.hey.cafe/share/comment/' + conversation + '/' + btoa(window.location.href) + ''; }</script>";
				$content_body.="<a href=\"javascript:opencommentwindow('".$conversation["id"]."');\"><div class='button'>Make Comment via Hey.Café</div></a>";
				$content_body.="</bubble>";
				
				//--Generate Comments!
				$comment_info_request=fetchurl("https://endpoint.hey.cafe/api/conversation_comments?query=".$articleid."&count=50");
				if ($comment_info_request!=false){
					$comment_info=json_decode($comment_info_request,true);
					if ($comment_info["system_api_error"]!=true){
						if (is_array($comment_info["response_data"]["comments"])){
							foreach ($comment_info["response_data"]["comments"] as $comment){
								//--Content
								if ($comment["info_hidden_cafe"]=="0" AND $comment["info_hidden_global"]=="0"){
									$contents=$comment["contents"];
									$timeago=generate_timestamp_ago($comment["date_created"],$body_info["system_api_timestamp"]);
									$author_name=$comment["account"]["name"];
									$author_alias=$comment["account"]["alias"];
									$author_avatar=$comment["account"]["avatar"];
									
									//--Generate
									$content_body.="<bubble id='comment_".$comment["id"]."' class='comment'><img class='optimizer_img' source='".$author_avatar."' src='".$system_blank_image."' alt='Profile image for ".$author_name."' style='float:left;margin-right:10px;margin-bottom:10px;width:60px;height:60px;border-radius:60px;'><a href='https://hey.cafe/p/".$author_alias."'>".$author_name."</a><div class='meta'>".$timeago.".</div><div class='content'><div id='format_comment_".$comment["id"]."'>".$contents."</div></div></bubble>";
									$content_body.="<action type='format_complex' element='format_comment_".$comment["id"]."'></action>";
								}
							}
						}else{
							$content_body.="<bubble style='text-align:center;'>We have no comments</bubble>";
						}
					}
				}
				
				
			}else{
				$content_body.="<bubble style='text-align:center;'>Sorry we cant find this post</bubble>";
			}
		}else{
			$content_body.="<bubble style='text-align:center;'>Sorry we cant find this post</bubble>";
		}
	}
}