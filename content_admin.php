<?php
//##############################################################
//##############################################################-- Admin Startup
//##############################################################

session_start();
clear_cache();
$admin_function=str_replace("/admin","",$system_uri);
$admin_auth=false;

//--Check login
if (isset($_SESSION['token'])){
	if ($_SESSION['token']==$system_config["heycafe_token"]){
		$admin_auth=true;
	}
}

//--Update fixes
if (!isset($system_config["theme_custom_menu"])){
	$system_config["theme_custom_menu"]=false;
	$system_config_update=true;
}
if (!isset($system_config["cache"])){
	$system_config["cache"]=false;
	$system_config_update=true;
}

//##############################################################
//##############################################################-- Functions
//##############################################################

function admin_login(){
	global $system_config;
	global $system_config_update;
	global $content_body;
	
	$token=makesafe($_GET["loginkey"]);
	
	$files_onnfo_request=fetchurl("https://endpoint.hey.cafe/api/account_loginkey?query=".$token."");
	if ($files_onnfo_request!=false){
		$files_onnfo=json_decode($files_onnfo_request,true);
		if ($files_onnfo["response_data"]!=""){
			//--Login
			if ($system_config["heycafe_account"]!=false){
				if ($files_onnfo["response_data"]["id"]==$system_config["heycafe_account"]){
					$_SESSION['token']=$token;
					$system_config["heycafe_token"]=$token;
					$system_config_update=true;
					header('Location: /admin');
				}else{
					$content_body.="<bubble style='text-align:center;'>Login failed.<BR><BR><a href='/admin'>Go back</a></bubble>";
				}
			}
			
			//--New setup
			if ($system_config["heycafe_account"]==false){
				$_SESSION['token']=$token;
				$system_config["heycafe_account"]=$files_onnfo["response_data"]["id"];
				$system_config["heycafe_token"]=$token;
				$system_config_update=true;
				header('Location: /admin');
			}
		}else{
			$content_body.="<bubble style='text-align:center;'>Login failed.<BR><BR><a href='/admin'>Go back</a></bubble>";
		}
	}else{
		$content_body.="<bubble style='text-align:center;'>We are not able to contact the Hey.Café endpoint.<BR><BR><a href='/admin'>Go back</a></bubble>";
	}
}

//##############################################################
//##############################################################-- Header!
//##############################################################

if ($admin_auth==true){
	$header_info_request=fetchurl("https://endpoint.hey.cafe/api/account_info?query=".$system_config["heycafe_account"]."");
	if ($header_info_request!=false){
		$header_info=json_decode($header_info_request,true);
		
		if ($header_info["system_api_error"]!=true){
			$data_alias=$header_info["response_data"]["alias"];
			$data_name=$header_info["response_data"]["name"];
			$data_bio=$header_info["response_data"]["bio"];
			$data_avatar=$header_info["response_data"]["avatar"];
			$data_header=$header_info["response_data"]["header"];
			
			$menu="";
			$menu.="<a href='/admin'>Main</a>";
			$menu.="<a href='/admin/content'>Content</a>";
			$menu.="<a href='/admin/design'>Design</a>";
			$menu.="<a href='/admin/update'>Update</a>";
			$menu.="<a href='/'>View Blog</a>";
			$menu.="<a href='/admin/logout'>Logout</a>";
			
			$content_header.="<bubble style='margin-top:100px;'><h2 style='margin-top:0px;'>Welcome ".$data_name."!</h2><menu>".$menu."</menu></bubble>";
		}
	}
}

//##############################################################
//##############################################################-- Generate Content
//##############################################################

//--Home
if ($admin_function=="" OR $admin_function=="/"){
	if ($admin_auth==false){
		if (isset($_GET["loginkey"])){
			admin_login();
		}else{
			$content_body.="<bubble style='margin-top:100px;text-align:center;'><script src='https://hey.cafe/application/external/sign_in_with.js' publisher='Hey.Café Simple Blog' reason='Blog admin and management' url='false'></script></bubble>";
		}
	}else{
		$content_body.="<bubble><h2 style='margin-top:0px;'>Main</h2>";
		$content_body.="This area allows you to manage your blog. To switch your content source visit the <a href='/admin/content'>content area</a> and <a href='/admin/design'>design</a> to change the look of your blog.";
		$content_body.="</bubble>";
	}
}

//--Content
if ($admin_function=="/content"){
	$content_body.="<bubble><h2 style='margin-top:0px;'>Content</h2>";
	
	//--Update source
	if (isset($_GET["type"])){
		$set_type=makesafe($_GET["type"]);
		$set_id=makesafe($_GET["id"]);
		
		if ($set_type=="account" OR $set_type=="cafe"){
			$system_config["heycafe_source_type"]=$set_type;
			$system_config["heycafe_source_id"]=$set_id;
			$system_config["heycafe_source_tag"]=false;
			$system_config_update=true;
		}
	}
	
	//--Update tag
	if (isset($_GET["tag"])){
		$set_tag=makesafe($_GET["tag"]);
		
		if ($set_tag!="false"){
			$system_config["heycafe_source_tag"]=$set_tag;
		}else{
			$system_config["heycafe_source_tag"]=false;
		}
		$system_config_update=true;
	}
	
	//--Load current source
	if ($system_config["heycafe_source_id"]!=false){
		$source_request=fetchurl("https://endpoint.hey.cafe/api/".$system_config["heycafe_source_type"]."_info?query=".$system_config["heycafe_source_id"]."");
		if ($source_request!=false){
			$source=json_decode($source_request,true);
			if ($source["system_api_error"]!=true){
				$tagmessage="";
				
				if ($system_config["heycafe_source_tag"]!=false){
					$tagmessage=" with the <strong>".$source["response_data"]["tags"]["".$system_config["heycafe_source_tag"].""]["emoji"]." ".$source["response_data"]["tags"]["".$system_config["heycafe_source_tag"].""]["name"]."</strong> tag";
				}
				
				if ($system_config["heycafe_source_type"]=="account"){
					$content_body.="<div style='overflow: hidden;'><img src='".$source["response_data"]["avatar"]."' style='float:left;max-width:55px;border-radius:55px;margin-right:10px;'><h3 style='margin-top: 0px;margin-bottom: 5px;'>Source is account</h3>The account ".$source["response_data"]["name"]." <a href='https://hey.cafe/p/".$source["response_data"]["alias"]."' target='_blank'>@".$source["response_data"]["alias"]."</a> is the current source of content for the blog".$tagmessage.".</div>";
				}
				if ($system_config["heycafe_source_type"]=="cafe"){
					$content_body.="<div style='overflow: hidden;'><img src='".$source["response_data"]["avatar"]."' style='float:left;max-width:55px;border-radius:55px;margin-right:10px;'><h3 style='margin-top: 0px;margin-bottom: 5px;'>Source is café</h3>The café ".$source["response_data"]["name"]." <a href='https://hey.cafe/c/".$source["response_data"]["alias"]."' target='_blank'>!".$source["response_data"]["alias"]."</a> is the current source of content for the blog".$tagmessage.".</div>";
				}
				
				
				if ($source["response_data"]["tags"]!==false){
					$content_body.="</bubble><bubble><h2 style='margin-top:0px;'>Content tags</h2>";
					foreach ($source["response_data"]["tags"] as $tag){
						$content_body.="<div style='overflow: hidden;margin-bottom: 15px;'><div style=\"font-size:2.3em;float:left;margin-right:15px;\">".$tag["emoji"]."</div><a href='/admin/content?tag=".$tag["id"]."'><h3 style='margin-top: 0px;margin-bottom: 5px;'>".$tag["name"]."</h3></a>Use only the content with this tag.</div>";
					}
				}
				
			}
		}
	}else{
		$content_body.="You have no content source selected, you can select your account or a café you are in on Hey.Café in the section below.";
	}
	$content_body.="</bubble>";
	
	$content_body.="<bubble><h2 style='margin-top:0px;'>Pick source</h2>";
	
	$source_me_request=fetchurl("https://endpoint.hey.cafe/api/account_info?query=".$system_config["heycafe_account"]."");
	if ($source_me_request!=false){
		$source_me=json_decode($source_me_request,true);
		if ($source_me["system_api_error"]!=true){
			$content_body.="<div style='overflow: hidden;margin-bottom: 15px;'><img src='".$source_me["response_data"]["avatar"]."' style='float:left;max-width:55px;border-radius:55px;margin-right:10px;'><a href='/admin/content?type=account&id=".$source_me["response_data"]["id"]."'><h3 style='margin-top: 0px;margin-bottom: 5px;'>".$source_me["response_data"]["name"]."</h3></a>Use the content from account <a href='https://hey.cafe/p/".$source_me["response_data"]["alias"]."' target='_blank'>@".$source_me["response_data"]["alias"]."</a>.</div>";
		}
	}
	
	//--Load cafes
	$source_cafes_request=fetchurl("https://endpoint.hey.cafe/api/account_cafes?query=".$system_config["heycafe_account"]."&count=200");
	if ($source_cafes_request!=false){
		$source_cafes=json_decode($source_cafes_request,true);
		if ($source_cafes["system_api_error"]!=true){
			if ($source_cafes["response_data"]["cafes"]!=false){
				foreach ($source_cafes["response_data"]["cafes"] as $cafe) {
					if ($cafe["you_admin"]==true){
						$content_body.="<div style='overflow: hidden;margin-bottom: 15px;'><img src='".$cafe["avatar"]."' style='float:left;max-width:55px;border-radius:55px;margin-right:10px;'><a href='/admin/content?type=cafe&id=".$cafe["id"]."'><h3 style='margin-top: 0px;margin-bottom: 5px;'>".$cafe["name"]."</h3></a>Use the content from café <a href='https://hey.cafe/c/".$cafe["alias"]."' target='_blank'>!".$cafe["alias"]."</a>.</div>";
					}
				}
			}
		}
	}
	
	$content_body.="</bubble>";
}

//--Design
if ($admin_function=="/design"){
	$content_body.="<bubble><h2 style='margin-top:0px;'>Design</h2>";
	$content_body.="You can update the look and feel of your blog here, like adding custom HTML content for a homepage. When you set homepage content your blog will be moved to a blog page automaticly.";
	$content_body.="</bubble>";
	
	//--UPDATES!!!!
	if (isset($_GET["update"])){
		if ($_GET["update"]=="colour"){
			$system_config["theme_colour"]=$_POST["content"];
			if ($_POST["content"]==""){ $system_config["theme_colour"]="rgb(96, 72, 244)"; }
			$system_config_update=true;
		}
		if ($_GET["update"]=="css"){
			$system_config["theme_custom_css"]=base64_encode($_POST["content"]);
			if ($_POST["content"]==""){ $system_config["theme_custom_css"]=false; }
			$system_config_update=true;
		}
		if ($_GET["update"]=="footer"){
			$system_config["theme_custom_footer"]=base64_encode($_POST["content"]);
			if ($_POST["content"]==""){ $system_config["theme_custom_footer"]=false; }
			$system_config_update=true;
		}
		if ($_GET["update"]=="homepage"){
			$system_config["theme_custom_homepage"]=base64_encode($_POST["content"]);
			if ($_POST["content"]==""){ $system_config["theme_custom_homepage"]=false; }
			$system_config_update=true;
		}
	}
	
	//--HTTPS
	$content_body.="<bubble><h2 style='margin-top:0px;'>HTTPS</h2>";
	
	//--turn on and off
	if (isset($_GET["https"])){
		if ($_GET["https"]=="on"){
			$system_config["theme_use_https"]=true;
			$system_config_update=true;
		}
		if ($_GET["https"]=="off"){
			$system_config["theme_use_https"]=false;
			$system_config_update=true;
		}
	}
	$content_body.="<div style='margin-bottom:15px;'>Ready for HTTPS? Awesome you can turn on HTTPS forced redirect here.</div>";
	if ($system_config["theme_use_https"]==false){
		$content_body.="<a href='/admin/design?https=on'>Redirect Status: Off</a>";
	}else{
		$content_body.="<a href='/admin/design?https=off'>Redirect Status: On</a>";
	}
	$content_body.="</bubble>";
	
	//--Theme Colour
	$content_body.="<bubble><h2 style='margin-top:0px;'>Colour</h2>";
	$content_body.="<form action='/admin/design?update=colour' method='post'><div style='text-align:center;'><input type='text' name='content' value='".$system_config["theme_colour"]."'> <input type='submit' name='submit' value='Update'></div></form>";
	$content_body.="</bubble>";
	
	//--CSS
	$content_body.="<bubble><h2 style='margin-top:0px;'>CSS</h2>";
	$content_body.="<form action='/admin/design?update=css' method='post'><div style='text-align:center;'><textarea name='content' style='min-height:300px;' placeholder='Enter custom data here...'>".base64_decode($system_config["theme_custom_css"])."</textarea><input type='submit' name='submit' value='Update'></div></form>";
	$content_body.="</bubble>";
	
	//--Footer
	$content_body.="<bubble><h2 style='margin-top:0px;'>Footer</h2>";
	$content_body.="<form action='/admin/design?update=footer' method='post'><div style='text-align:center;'><textarea name='content' style='min-height:300px;' placeholder='Enter custom data here...'>".base64_decode($system_config["theme_custom_footer"])."</textarea><input type='submit' name='submit' value='Update'></div></form>";
	$content_body.="</bubble>";
	
	//--Homepage
	$content_body.="<bubble><h2 style='margin-top:0px;'>Custom Homepage Content</h2>";
	$content_body.="<form action='/admin/design?update=homepage' method='post'><div style='text-align:center;'><textarea name='content' style='min-height:300px;' placeholder='Enter custom data here...'>".base64_decode($system_config["theme_custom_homepage"])."</textarea><input type='submit' name='submit' value='Update'></div></form>";
	$content_body.="</bubble>";
}

//--Logout
if ($admin_function=="/logout"){
	$content_body.="<bubble><h2 style='margin-top:0px;'>Logout</h2>You have been logged out of the admin area.</bubble>";
	$_SESSION['token']="logout";
	header('Location: /');
}

//--Update
if ($admin_function=="/update"){
	//--Fetch file
	$downloadworked=true;
	$source = urldecode("https://gitlab.com/heycafe/simple-blog/-/archive/main/simple-blog-main.zip?version=".time()."");
	$dest = "update-package.zip";
	
	$options  = array('http' => array('user_agent' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.2 Safari/605.1.15'));
	$context  = stream_context_create($options);
	
	if ($downloadworked==true){
		$response = file_get_contents($source, false, $context);
		if($response === false){
			$downloadworked=false;
			$content_body.="<bubble style='text-align:center;'>The update file download failed.</bubble>";
		}
	}
	
	if ($downloadworked==true){
		$save = file_put_contents($dest, $response);
		if($save === false){
			$downloadworked=false;
			$content_body.="<bubble style='text-align:center;'>The update file download failed.</bubble>";
		}
	}
	
	if ($downloadworked==true){
		$content_body.="<bubble><h2 style='margin-top:0px;'>Update</h2>";
		unlink("script_combined_compressed.js");
		$files_leftath = 'update-package.zip';
		$zip = new ZipArchive;
		$files_on=0;
		$files_left=500;
		if ($zip->open($files_leftath) === true){
			$files=$zip->numFiles;
			while ($files_left >= 1){
				$originalfilename = $zip->getNameIndex($files_on);
				if ($originalfilename!=""){
					$fileinfo = pathinfo($originalfilename);
					$filename = substr($originalfilename, strpos($originalfilename, "/") + 1);
					
					$move=true;
					if ($filename=="config.json"){ $move=false; }
					if ($filename==".nova/"){ $move=false; }
					if ($filename==".nova/Configuration.json"){ $move=false; }
					
					
					//--Config update!
					if ($filename=="config.json"){
						$source=file_get_contents("zip://".$files_leftath."#".$originalfilename."");
						if ($source!=""){
							$source=json_decode($source,true);
							foreach ($source as $index => $value){
								if (isset($system_config["".$index.""])){
									$content_body.="<div>Config file already has ".$index.".</div>";
								}else{
									$system_config["".$index.""]=$value;
									$content_body.="<div>The config value for ".$index." has been added to the config file.</div>";
									$system_config_update=true;
								}
							}
						}else{
							$content_body.="<div><strong>Error!</strong> Cant read config data.</div>";
						}
					}
					
					if ($move==true){
						$copy=false;
						if (file_exists($filename)){
							if (!is_dir($filename)){
								unlink($filename);
								$copy=true;
							}
						}else{
							$copy=true;
						}
						if ($copy==true){
							if ($filename!=""){
								copy("zip://".$files_leftath."#".$originalfilename, $filename);
								$content_body.="<div>File ".$filename." updated.</div>";
							}
						}
					}else{
						if (file_exists($filename)){
							//$content_body.="<div>File ".$filename." skiped with data [i:".$files_on.", p:".$files_left."] - we dont replace this file.</div>";
						}
					}
					$files_left=$files_left-1;
					$files_on=$files_on+1;
				}else{
					$files_left=0;
					$content_body.="<BR><BR><div>Files are all updated.</div><BR><BR>You can now go <a href='/admin'>back to admin</a>.";
				}
			}
			$zip->close();
			unlink($files_leftath);
		}
		$content_body.="</bubble>";
	}
}