var pageisloaded=false;

function getHTTP(url, callback, maxtime){
	var xhr = new XMLHttpRequest();
	var timeout=10000;
	if (maxtime!=undefined){
		timeout=maxtime;
	}
	
	xhr.timeout = timeout;
	xhr.withCredentials = true;
	
	xhr.onreadystatechange = function(){
		if(xhr.readyState==4 && xhr.status==200){
			content = xhr.responseText;
			callback(content);
		}else{
			if(xhr.readyState==4 && xhr.status!=200){
				callback("{ \"fail\": \"unknown\", \"system_api_error\": true }");
			}
		}
	}
	
	xhr.ontimeout = function() {
		callback("{ \"fail\": \"timeout\", \"system_api_error\": true }");
	}
	xhr.open("GET", url, true);
	if (settings_authsession!=null){
		xhr.setRequestHeader("authsession", settings_authsession);
	}
	
	xhr.send();
}

function afterloadRun(contents){
	var matches = contents.match(/\<attachment id=\'file\_([^\n\']+)\'\>/gi);
	forEach(matches, function(id){
		var data=matches["" + id + ""];
		var result = data.match(/id=\'file\_([^\n\']+)\'/);
		var filename=result[1];
		if (document.getElementById("bottom_file_" + filename + "")){
			document.getElementById("file_" + filename + "").innerHTML=document.getElementById("bottom_file_" + filename + "").innerHTML;
			document.getElementById("bottom_file_" + filename + "").innerHTML="";
		}
	});
}

function embedSystemRun(contents){
	
	var matches = contents.match(/<a([^\<\>]+)href\=[\'\"]([^\<\n\s\'\"]+)[\'\"]([^\<\>]+)>(.*?)<\/a>/gi);
	forEach(matches, function(id){
		var data=matches["" + id + ""];
		var result = data.match(/href\=[\'\"]([^\<\n\s\'\"]+)[\'\"]/);
		var link=result[1];
		var embed=embedSystemGenerate(link);
		if (embed!=false){
			contents=contents.replace(data, embed);
		}
	});
	
	return contents;
}

function embedSystemGenerate(url){
	var embed=false;
	
	//--Youtube embed
	if (url.includes("youtube.com") || url.includes("youtu.be")){
	  if (url.includes("/watch?") || url.includes("embed/") || url.includes("youtu.be/")){
		var foundid =url.match(/^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#&?]*).*/);
		var videoid=(foundid&&foundid[7].length==11)? foundid[7] : false;
		
		if (videoid==false){
		  var foundidtwo=url.match(/^.*youtu.be\/([^#&?]*).*/);
		  var videoid=(foundidtwo&&foundidtwo[1].length==11)? foundidtwo[1] : false;
		}
		
		if (videoid!=false){
			embed='<div class="aspectcase sixteenbynine"><div class="content"><iframe allow="autoplay *; encrypted-media *; fullscreen *" style="width:100%;height:100%;border:0px;border-radius:6px;" src="https://www.youtube.com/embed/' + videoid + '"></iframe></div></div>';
		}
	  }
	}
	
	//--Twitter embed
	if (url.includes("twitter.com")){
	  if (url.includes("/status/")){
		var foundid=url.match(/^.*twitter.com\/([^#&?]*)\/status\/([0-9]*)/);
		var id=foundid[2];
		embed='<div id="twitter_embed_' + id + '"><blockquote class="twitter-tweet"><a href="https://twitter.com/x/status/' + id + '">' + url + '</a></blockquote></div>';
	    twitterEmbedScript('twitter_embed_' + id + '');
	  }
	}
	
	//--Hey.Café Conversation Embed
	if (url.includes("hey.cafe/conversation/")){
		var foundid=url.match(/\/conversation\/([^\<\n\s\'\"]+)/);
		var id=foundid[1];
		embed='<iframe frameborder="0" style="width:100%;max-width:680px;margin:0px auto;overflow:hidden;background:transparent;display:block;opacity: 0;" onload="this.style.opacity = 1;" sandbox="allow-forms allow-popups allow-same-origin allow-scripts" src="https://beta.hey.cafe/embed/conversation/' + id + '" id="heycafe_conversation_' + id + '"></iframe>';
		heycafeEmbedScript('heycafe_conversation_' + id + '');
	}
	
	return embed;
}

function heycafeEmbedScript(id){
	if (document.getElementById(id)){
		var element=document.createElement('script');
		element.text = '{ let hey_framename="' + id + '"; let hey_iframe = document.querySelector("#" + hey_framename); window.addEventListener(\'message\', function(e){ if (e.source == hey_iframe.contentWindow){ if (e.origin.includes("hey.cafe")){ let hey_message = e.data; hey_iframe.style.height = hey_message.height + \'px\'; hey_offset=document.documentElement.scrollTop-hey_iframe.offsetTop; if (hey_offset<=0){ hey_offset=0; } let hey_response = { type: "embed_position", value: hey_offset }; e.source.postMessage(hey_response, e.origin); }}} , false); }';
		document.body.appendChild(element);
	}else{
		setTimeout("heycafeEmbedScript('" + id + "')", 200);
	}
}

function twitterEmbedScript(id){
	if (document.getElementById(id)){
		var element=document.createElement('script');
		element.src = 'https://platform.twitter.com/widgets.js';
		document.body.appendChild(element);
	}else{
		setTimeout("twitterEmbedScript('" + id + "')", 200);
	}
}

document.addEventListener("DOMContentLoaded", function(){
	if (document.getElementsByTagName("ACTION")){
		runactions = document.getElementsByTagName("ACTION");
		[].slice.call(runactions).forEach(function(item){
			var done=false;
			var type=item.getAttribute("type");
			
			if (type=="format_simple"){
				var element=item.getAttribute("element");
				if (document.getElementById(element)){
					
					var contents=document.getElementById(element).innerHTML;
					contents=formatBasicText(contents);
					contents=formatBasicLinks(contents);
					contents=emojiReplace(contents);
					contents=formatInternalLinksExt(contents);
					document.getElementById(element).innerHTML=contents;
				}
				done=true;
			}
			
			if (type=="format_complex"){
				var element=item.getAttribute("element");
				if (document.getElementById(element)){
					
					var contents=document.getElementById(element).innerHTML;
					contents=formatComplexText(contents);
					contents=formatComplexLinks(contents);
					contents=emojiReplace(contents);
					contents=formatLargerEmoji(contents);
					contents=formatCompexFixes(contents);
					contents=formatInternalLinksExt(contents);
					contents=embedSystemRun(contents);
					
					document.getElementById(element).innerHTML=contents;
					
					afterloadRun(contents);
				}
				done=true;
			}
			
			if (type=="inview_load_metadata"){
				var send_element=item.getAttribute("element");
				var send_url=item.getAttribute("url");
				metadata(send_element,send_url);
				done=true;
			}
			
			if (done==true){
				item.remove();
			}
		});
	}
	
	
	pageisloaded=true;
	setTimeout("optimizerLazyScrollCheck();", 200);
});

document.addEventListener('scroll', function(e){
	if (pageisloaded==true){
		optimizerLazyScrollCheck();
	}
});

function metadata(element,url){
	if (document.getElementById(element)){
		document.getElementById(element).innerHTML="<div class='loading'></div>";
		getHTTP("https://endpoint.hey.cafe/get_bot_website_meta?query=" + b64EncodeUnicode(url) + "",function(result) {
			var response = decodeJSON(result);
			if (response["system_api_error"]==false){
				var data=response["response_data"];
				
			}
		});
	}
}

window.onkeyup = function(e){
	var event = e.which || e.keyCode || 0;
	//--ESC Key
	if (event == 27) {
		window.location.href = '/admin';
	}
}