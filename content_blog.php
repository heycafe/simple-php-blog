<?php

if ($system_config["heycafe_source_id"]!=false){
	$offset=0;
	$page=1;
	if (isset($_GET["page"])){
		$page=intval($_GET["page"]);
		$offset=($page*10)-10;
	}
	
	$tag="";
	if ($system_config["heycafe_source_tag"]!=false){
		$tag="&tag=".$system_config["heycafe_source_tag"]."";
	}
	
	$body_info_request=fetchurl("https://endpoint.hey.cafe/api/".$system_config["heycafe_source_type"]."_conversations?query=".$system_config["heycafe_source_id"]."&start=".$offset."&count=11".$tag."");
	if ($body_info_request!=false){
		$number=0;
		$hasmore=false;
		$body_info=json_decode($body_info_request,true);
		
		if ($body_info["system_api_error"]!=true){
			if (is_array($body_info["response_data"]["conversations"])){
				
				foreach ($body_info["response_data"]["conversations"] as $conversation){
					$number=$number+1;
					if ($number<=10){
							
						//--Content
						$contents=$conversation["contents"];
						$footer="";
						
						//--Check for source MD files
						if ($conversation["attachments"]!=false){
							foreach ($conversation["attachments"] as $attach){
								if ($attach["type"]=="file"){
									if (strpos($attach["file"],".md") !== false){
										$contents=makesafe(fetchurl($attach["file"]));
									}
								}
							}
						}
						
						//--get title and remove header element if same
						$title=generate_title($contents);
						$contents = trim(str_replace("## ".$title." ##","",$contents));
						
						//--get first line of content
						$contents_new=generate_firstline($contents);
						if ($contents_new!=$contents){
							$contents=$contents_new;
							$footer="<a href='/article/".$conversation["id"]."'><div class='button'>Read the full post</div></a>";
						}
						
						//--Timestamp
						$timeago=generate_timestamp_ago($conversation["date_created"],$body_info["system_api_timestamp"]);
						
						//--Author
						$author=$conversation["account"]["name"];
						
						//--get header
						$headerimage="";
						if ($conversation["attachments"]!=false){
							foreach ($conversation["attachments"] as $attach){
								if ($attach["primary"]=="1" AND $attach["type"]=="image"){
									$headerimage="<img class='optimizer_img' source='".$attach["file"]."' src='".$system_blank_image."' alt='Preview image for article'>";
								}
								if ($attach["primary"]=="0"){
									$footer="<a href='/article/".$conversation["id"]."'><div class='button'>Read the full post</div></a>";
								}
							}
						}
						
						//--Generate
						$content_body.="<bubble id='post_".$conversation["id"]."' class='article'><a href='/article/".$conversation["id"]."'><h2>".$title."</h2></a><div class='meta'>".$timeago." by ".$author.".</div><div class='headerimage float'>".$headerimage."</div><div class='content'><div id='format_".$conversation["id"]."'>".$contents."</div>".$footer."</div></bubble>";
						$content_body.="<action type='format_complex' element='format_".$conversation["id"]."'></action>";
					}else{
						$hasmore=true;
					}
				}
				
				//--Pages
				if ($hasmore==true OR $offset!=0){
					$content_body.="<bubble style='overflow:hidden;'>";
					if ($hasmore==true){
						$content_body.="<div style='float:right;'><a href='/blog?page=".($page+1)."'><div class='button'>Next Page</div></a></div>";
					}
					if ($offset!=0){
						$content_body.="<div style='float:left;'><a href='/blog?page=".($page-1)."'><div class='button'>Last Page</div></a></div>";
					}
					$content_body.="</bubble>";	
				}
				
			}else{
				$content_body.="<bubble style='text-align:center;'>Sorry we cant find any blog posts</bubble>";
			}
		}
	}
}