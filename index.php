<?php

set_time_limit(90);
ignore_user_abort(true);
ini_set('output_buffering', 0);
ini_set('display_errors', '0');
ini_set('date.timezone', 'Etc/GMT+8'); //--Is America/Vancouver With DTS AKA winter time in BC
ini_set('memory_limit','32M');

//##############################################################
//##############################################################-- Startup lets check a few things first!
//##############################################################

$system_page_type="404";
$system_page_id="";
$system_htaccess=false;
$system_ip=gettrueip();
$system_useragent=makesafe($_SERVER['HTTP_USER_AGENT']);
$system_domain=makesafe($_SERVER['HTTP_HOST']);
$system_timestamp=date('YmdHis');
$system_https=false;
$system_http="http://";
$system_theme="";
$system_config="";
$system_cache="";

$system_blank_image="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAoAAAAFoCAQAAABtO4LaAAADc0lEQVR42u3UMQEAAAgDINc/9IzhIYQg7QC8FAECAgQQIIAAAQQIIEAAAQIIEECAAAIEECCAAAEECCBAAAECCBBAgAACBBAggAABBAggQAABAggQQIAAAgQQIIAAAQQICBBAgAACBBAggAABBAggQAABAggQQIAAAgQQIIAAAQQIIEAAAQIIEECAAAIEECCAAAEECCBAAAECCBBAgAACBBAggAABAQIIEECAAAIEECCAAAEECCBAAAECCBBAgAACBBAggAABBAggQAABAggQQIAAAgQQIIAAAQQIIEAAAQIIEECAAAIEBChAQIAAAgQQIIAAAQQIIEAAAQIIEECAAAIEECCAAAEECCBAAAECCBBAgAACBBAggAABBAggQAABAggQQIAAAgQQIIAAAQECCBBAgAACBBAggAABBAggQAABAggQQIAAAgQQIIAAAQQIIEAAAQIIEECAAAIEECCAAAEECCBAAAECCBBAgAACBBAgIEAAAQIIEECAAAIEECCAAAEECCBAAAECCBBAgAACBBAggAABBAggQAABAggQQIAAAgQQIIAAAQQIIEAAAQIIEECAgAAFCAgQQIAAAgQQIIAAAQQIIEAAAQIIEECAAAIEECCAAAEECCBAAAECCBBAgAACBBAggAABBAggQAABAggQQIAAAgQQICBAAAECCBBAgAACBBAggAABBAggQAABAggQQIAAAgQQIIAAAQQIIEAAAQIIEECAAAIEECCAAAEECCBAAAECCBBAgAACBAQIIEAAAQIIEECAAAIEECCAAAEECCBAAAECCBBAgAACBBAggAABBAggQAABAggQQIAAAgQQIIAAAQQIIEAAAQIIEBCgAAEBAggQQIAAAgQQIIAAAQQIIEAAAQIIEECAAAIEECCAAAEECCBAAAECCBBAgAACBBAggAABBAggQAABAggQQIAAAgQECCBAAAECCBBAgAACBBAggAABBAggQAABAggQQIAAAgQQIIAAAQQIIEAAAQIIEECAAAIEECCAAAEECCBAAAECCBBAgIAAAQQIIEAAAQIIEECAAAIEECCAAAEECCBAAAECCBBAgAACBBAggAABBAggQAABAggQQIAAAgQQIIAAAQQIIEAAAQICFCAgQAABAggQQIAAAgQQIIAAAQQIIEAAAQIIEECAAAIEECDAnQXyKM63O/4L6wAAAABJRU5ErkJggg==";

$meta_title="";
$meta_description="";
$meta_image="";
$meta_name="";
$meta_icon="";

$content_head="";
$content_header="";
$content_body="";
$content_footer="";
$content_extra="";
$content_script="";

$account_alias="";
$account_name="";
$account_bio="";
$account_avatar="";
$account_header="";

$compress_keys_used=array();

if (strpos($_SERVER['REQUEST_URI'], '?') !== false){
	$system_uri=makesafe(substr($_SERVER['REQUEST_URI'], 0, strrpos( $_SERVER['REQUEST_URI'], "?")));
}else{
	$system_uri=makesafe($_SERVER['REQUEST_URI']);
}
$system_uri_full=makesafe($_SERVER['REQUEST_URI']);
if (check_domain_ssl_active()==true){
	$system_https=true;
}

//##############################################################
//##############################################################-- Page Settings
//##############################################################

if ($system_uri=="/"){ $system_page_type="home"; }
if (substr($system_uri, 0, 6) === "/admin"){ $system_page_type="admin"; }
if (substr($system_uri, 0, 5) === "/blog"){ $system_page_type="blog"; }
if (substr($system_uri, 0, 8) === "/article"){ $system_page_type="article"; }

//##############################################################
//##############################################################-- Load files that are user editable for the settings, and the theme
//##############################################################

$system_theme=file_get_contents("./theme.html", FILE_USE_INCLUDE_PATH);
$system_config=json_decode(file_get_contents("./config.json", FILE_USE_INCLUDE_PATH),true);
$system_cache=json_decode(file_get_contents("./cache.json", FILE_USE_INCLUDE_PATH),true);
$system_config_update=false;
$system_cache_update=false;

if (!file_exists("./.htaccess")){
	file_put_contents("./.htaccess", base64_decode("UmV3cml0ZUVuZ2luZSBvbgpSZXdyaXRlQ29uZCAle1JFUVVFU1RfVVJJfSAhXi9pbmRleC5waHAkClJld3JpdGVDb25kICV7UkVRVUVTVF9VUkl9ICFcLihnaWZ8anBlP2d8cG5nfGNzc3xqc3xqc29ufHR4dCkkClJld3JpdGVSdWxlIC4qIC9pbmRleC5waHAgW05DLEwsUVNBXQ=="));
}

if ($system_config["theme_use_https"]==true){ $system_http="https://"; }else{ $system_http="http://"; }

//##############################################################
//##############################################################-- Clear global cache
//##############################################################

$cachexpiretime=date('YmdHis',(time()-7200)); //2 hours
if ($system_cache["timestamp"]<=$cachexpiretime){
	clear_cache();
}

//##############################################################
//##############################################################-- Functions
//##############################################################

function makesafe($d,$type="basic"){
	$d = str_replace("\t","~~",$d);
	$d = str_replace("\r","",$d);
	$d = str_replace("\n","  ",$d);
	$d = str_replace("|","&#124;",$d);
	$d = str_replace("\\","&#92;",$d);
	$d = str_replace("(c)","&#169;",$d);
	$d = str_replace("(r)","&#174;",$d);
	$d = str_replace("\"","&#34;",$d);
	$d = str_replace("'","&#39;",$d);
	$d = str_replace("<","&#60;",$d);
	$d = str_replace(">","&#62;",$d);
	$d = str_replace("+","&#43;",$d);
	$d = str_replace("`","&#96;",$d);
	return $d;
}

function convert_timestring($timestamp){
	$y=substr($timestamp, 0, 4); //[2018]0218105347
	$m=substr($timestamp, 4, 2); //[2018][02]18105347
	$d=substr($timestamp, 6, 2); //[2018][02][18]105347
	$h=substr($timestamp, 8, 2); //[2018][02][18][10]5347
	$mi=substr($timestamp, 10, 2); //[2018][02][18][10][53]47
	$s=substr($timestamp, 12, 2); //[2018][02][18][10][53][47]
	$stringdate="$y-$m-$d $h:$mi:$s";
	return $stringdate;
}

function clear_cache(){
	global $system_timestamp;
	global $system_cache;
	global $system_cache_update;
	
	$system_cache="";
	$system_cache=array();
	$system_cache["timestamp"]=$system_timestamp;
	$system_cache_update=true;
}

function check_domain_ssl_active(){
	if (isset($_SERVER['HTTP_CF_VISITOR'])) {
		$cf_visitor = json_decode($_SERVER['HTTP_CF_VISITOR']);
		if (isset($cf_visitor->scheme) && $cf_visitor->scheme == 'https') {
			return true;
		}
	}else{
		if (isset($_SERVER['HTTPS'])) {
			return true;
		}else{
			return false;
		}
	}
}

function gettrueip(){
	$ip="0.0.0.0";
	if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])){ if (filter_var($_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP)){ if ($ip=="0.0.0.0"){ $ip=$_SERVER['HTTP_X_FORWARDED_FOR']; }}}
	if (isset($_SERVER['HTTP_CF_CONNECTING_IP'])){ if (filter_var($_SERVER['HTTP_CF_CONNECTING_IP'], FILTER_VALIDATE_IP)){ if ($ip=="0.0.0.0"){ $ip=$_SERVER['HTTP_CF_CONNECTING_IP']; }}}
	if (isset($_SERVER['Cf-Connecting-IP'])){ if (filter_var($_SERVER['Cf-Connecting-IP'], FILTER_VALIDATE_IP)){ if ($ip=="0.0.0.0"){ $ip=$_SERVER['Cf-Connecting-IP']; }}}
	if (isset($_SERVER['HTTP_CLIENT_IP'])){ if (filter_var($_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP)){ if ($ip=="0.0.0.0"){ $ip=$_SERVER['HTTP_CLIENT_IP']; }}}
	if (isset($_SERVER['REMOTE_ADDR'])){ if (filter_var($_SERVER['REMOTE_ADDR'], FILTER_VALIDATE_IP)){ if ($ip=="0.0.0.0"){ $ip=$_SERVER['REMOTE_ADDR']; }}}
	return $ip;
}

function fetchurl($url) {
	global $system_useragent;
	$process = curl_init($url);
	curl_setopt($process, CURLOPT_HEADER, 0);
	curl_setopt($process, CURLOPT_TIMEOUT, 30);
	curl_setopt($process, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($process, CURLOPT_FOLLOWLOCATION, 1);
	curl_setopt($process, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($process, CURLOPT_USERAGENT, $system_useragent);
	curl_setopt($process, CURLOPT_FAILONERROR, true);
	curl_setopt($process, CURLOPT_AUTOREFERER, true);
	curl_setopt($process, CURLOPT_MAXREDIRS, 7);
	
	$return = curl_exec($process);
	if (curl_error($process)){
		return false;
	}
	curl_close($process);
	return $return;
}

function compress($str){
	global $compress_keys_used;
	$str=str_replace("  ", " ", $str);
	$str=str_replace("  ", " ", $str);
	$str=str_replace("  ", " ", $str);
	$str=str_replace("  ", " ", $str);
	$str=str_replace("  ", " ", $str);
	$str=str_replace("  ", " ", $str);
	$str=preg_replace('/(?<!\S)\/\/\s*[^\r\n]*/', '', $str);
	$str=preg_replace('/((?=^)(\s*))|((\s*)(?>$))/', '', $str);
	$str=preg_replace('/[\t]+/', '', trim($str));
	$str=preg_replace('/(?<!\S)\/\/\--s*[^\r\n]*/', '', $str);
	$str=preg_replace('/(?<!\S)\/\/\##s*[^\r\n]*/', '', $str);
	$str=preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $str);
	$str=preg_replace("/\n /", "\n", $str);
	$str=preg_replace("/}\nvar/", "} var", $str);
	$str=preg_replace("/}\nsetTimeout/", "} setTimeout", $str);
	$str=preg_replace("/{\nsetTimeout/", "{ setTimeout", $str);
	$str=preg_replace("/\nvar/", " var", $str);
	$str=preg_replace("/\rvar/", " var", $str);
	$str=preg_replace("/\n var/", " var", $str);
	$str=preg_replace("/\r var/", " var", $str);
	$str=preg_replace("/\nif/", " if", $str);
	$str=preg_replace("/\rif/", " if", $str);
	$str=preg_replace("/\n if/", " if", $str);
	$str=preg_replace("/\r if/", " if", $str);
	$str=preg_replace("/\nelse/", " else", $str);
	$str=preg_replace("/\nwhile/", " while", $str);
	$str=preg_replace("/\rwhile/", " while", $str);
	$str=preg_replace("/;\n/", "; ", $str);
	$str=preg_replace("/{\n/", "{ ", $str);
	$str=preg_replace("/\n {/", "{ ", $str);
	$str=preg_replace("/\n{/", "{ ", $str);
	$str=preg_replace("/\nelse{/", "else{", $str);
	$str=preg_replace("/\nelse {/", "else{", $str);
	$str=preg_replace("/; \n/", "; ", $str);
	$str=preg_replace("/{ \n/", "{ ", $str);
	$str=preg_replace("/\n}/", " }", $str);
	$str=preg_replace("/\n }/", " }", $str);
	$str=preg_replace("/\r}/", " }", $str);
	$str=preg_replace("/\r }/", " }", $str);
	$str=preg_replace("/\nreturn/", " return", $str);
	$str=preg_replace("/}\nreturn/", "} return", $str);
	
	$str=preg_replace("/}\nfunction/", "} function", $str);
	$str=preg_replace("/}\rfunction/", "} function", $str);
	$str=preg_replace("/\n\\\$/", " $", $str);
	$str=preg_replace("/\ndocument./", " document.", $str);
	
	$str=str_replace("//--#############################################################", "", $str);

	//Short Fixes
	$str=str_replace("  ", " ", $str);
	$str=str_replace("  ", " ", $str);
	$str=str_replace("  ", " ", $str);
	$str=str_replace("  ", " ", $str);
	$str=str_replace("} else {", "}else{", $str);
	$str=str_replace(") {", "){", $str);
	$str=str_replace("; }", ";}", $str);
	$str=str_replace("\" + ", "\"+", $str);
	$str=str_replace(" + \"", "+\"", $str);
	$str=str_replace("' + ", "'+", $str);
	$str=str_replace(" + '", "+'", $str);
	$str=str_replace("} }", "}}", $str);
	$str=str_replace(" == ", "==", $str);
	$str=str_replace("  ", " ", $str);
	$str=str_replace("  ", " ", $str);
	
	//--Function name shorting
	preg_match_all('/function[\s\n]+(\S+)[\s\n]*\(/', $str, $matches);
	foreach ($matches[1] as &$value){
		$code=compress_makekey();
		
		$str=str_replace("".$value."(","".$code."(", $str);
		
		if ($value=="forEach"){
			$str=str_replace(".".$code."(",".forEach(", $str);
		}
		
		if ($value=="pageShown" OR $value=="pageHidden"){
			$str=str_replace(''.$value.',',''.$code.',', $str);
		}
		
	}
	
	//--Additions to compression with replacing function calls with shorter calls
	$str=str_replace("document.getElementById(", "aa(", $str);
	$str='function aa(s) { return document.getElementById(s); } '.$str.'';
	$str=str_replace("document.getElementsByTagName(", "ab(", $str);
	$str='function ab(s) { return document.getElementsByTagName(s); } '.$str.'';
	$str=str_replace("document.getElementsByClassName(", "ac(", $str);
	$str='function ac(s) { return document.getElementsByClassName(s); } '.$str.'';
	$str=str_replace("document.querySelector(", "ad(", $str);
	$str='function ad(s) { return document.querySelector(s); } '.$str.'';
	$str=str_replace("encodeURIComponent(", "ae(", $str);
	$str='function ae(s) { return encodeURIComponent(s); } '.$str.'';
	
	return $str;
}

function compress_makekey(){
	global $compress_keys_used;
	$code=codegenerate(3,"text");
	if (isset($compress_keys_used["".$code.""])==false){
		$compress_keys_used["".$code.""]=true;
		return "fu_".$code."";
	}else{
		return compress_makekey();
	}
}

function codegenerate($length,$type){
	if ($length<=0){
		$length=10;
	}
	if ($type=="normal"){
		$characters = '0123456789abcdefghijklmnopqrstuvwxyz';
	}
	if ($type=="text"){
		$characters = 'abcdefghijklmnopqrstuvwxyz';
	}
	if ($type=="simple"){
		$characters = '0123456789abcabcabc';
	}
	if ($type=="password"){
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-_-_-_-_$$%%#@@!!((*&&^^%%@&^$T^(!++++';
	}
	$randomString = '';
	for ($i = 0; $i < $length; $i++) {
		$randomString .= $characters[rand(0, strlen($characters) - 1)];
	}
	return $randomString;
}

function readableColour($bg){
	$pieces = explode(",", $bg);
	$r = $pieces[0];
	$g = $pieces[1];
	$b = $pieces[2];
	
	$brightness = round((($r * 299) + ($g * 587) + ($b * 114)) / 1000);

	if($brightness > 125){
		return '0,0,0';
	}else{
		return '255,255,255';
	}
}

//##############################################################
//##############################################################-- Https Redirect
//##############################################################

if ($system_https==false){
	if ($system_config["theme_use_https"]==true){
		header('Location: https://'.$_SERVER['HTTP_HOST'].''.$_SERVER['REQUEST_URI'].'');
		die();
	}
}

//##############################################################
//##############################################################-- WWW Redirect, NO!
//##############################################################

if (strpos($_SERVER['HTTP_HOST'], 'www.') !== false){
	header('Location: '.$system_http.''.str_replace('www.','', $_SERVER['HTTP_HOST']).''.$_SERVER['REQUEST_URI'].'');
}

//##############################################################
//##############################################################-- Compile Scripts
//##############################################################

if (file_exists("script_combined_compressed.js")==false){
	ob_start();
	
	//--Download scripts we need
	echo fetchurl("https://beta.hey.cafe/application/function/core.js?cache=".time()."");
	echo fetchurl("https://beta.hey.cafe/application/function/emoji.js?cache=".time()."");
	echo fetchurl("https://beta.hey.cafe/application/function/formating.js?cache=".time()."");
	echo fetchurl("https://beta.hey.cafe/application/function/optimizer.js?cache=".time()."");
	
	include("script_theme.js");
	
	$output = ob_get_contents(); //Gives whatever has been "saved"
	ob_end_clean(); //Stops saving things and discards whatever was saved
	ob_flush();
	
	$file_mini = fopen(getenv("DOCUMENT_ROOT") . "/script_combined_compressed.js","w");
	fwrite($file_mini,compress($output));
	fclose($file_mini);
}

$contents = file_get_contents("./script_combined_compressed.js", FILE_USE_INCLUDE_PATH);
$content_script="<script>".$contents."</script>";

//##############################################################
//##############################################################-- Content Functions
//##############################################################

function generate_title($content){
	$return=false;
	
	//--Use heading format as title first!
	$findheading = preg_match_all("/(\#\#\s)([^\n]+)(\s\#\#)[^#]/", $content, $headingfind);
	foreach ($headingfind[2] as $ht){
		$return=$ht;
	}
	
	//--Use first word as heading if nothing else
	$findheading = preg_match_all("/([^\s]+)/", $content, $headingfind);
	foreach ($headingfind[0] as $ht){
		if ($return==false){
			$return=$ht;
		}
	}
	
	return $return;
}

function generate_firstline($content){
	$return=false;
	$content = str_replace("    ","\n\n",$content);
	
	$findheading = preg_match_all("/(.[^\n\t]+)/", $content, $headingfind);
	foreach ($headingfind[0] as $ht){
		if ($return==false){
			$return=$ht;
		}
	}
	
	return $return;
}

function generate_timestamp_string($string,$timestamp){
	$timestampmake = strtotime(convert_timestring($timestamp));
	return date($string,$timestampmake);	
}

function generate_timestamp_ago($timestamp,$timenow,$full=false){
	$now = new DateTime(convert_timestring($timenow));
	$ago = new DateTime(convert_timestring($timestamp));
	$diff = $now->diff($ago);
	
	$diff->w = floor($diff->d / 7);
	$diff->d -= $diff->w * 7;
	
	$string = array(
	'y' => 'year',
	'm' => 'month',
	'w' => 'week',
	'd' => 'day',
	'h' => 'hour',
	'i' => 'minute',
	's' => 'second',
	);
		foreach ($string as $k => &$v){
		if ($diff->$k) {
			$v=$diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
		} else {
			unset($string[$k]);
		}
	}
	
	if ($full==false){
		$string = array_slice($string, 0, 1);
	}
	return $string ? implode(', ', $string) . ' ago' : 'just now';
}

//##############################################################
//##############################################################-- Fetch Base Content
//##############################################################

if ($system_config["heycafe_source_id"]!=false){
	if (!isset($system_cache["ca_res_source"])){
		$source_info_request=fetchurl("https://endpoint.hey.cafe/api/".$system_config["heycafe_source_type"]."_info?query=".$system_config["heycafe_source_id"]."");
		if ($source_info_request!=false){
			$source_info=json_decode($source_info_request,true);
			
			if ($header_info["system_api_error"]!=true){
				$account_alias=$source_info["response_data"]["alias"];
				$account_name=$source_info["response_data"]["name"];
				$account_bio=$source_info["response_data"]["bio"];
				$account_avatar=$source_info["response_data"]["avatar"];
				$account_header=$source_info["response_data"]["header"];
				
				//--Cache save
				$system_cache["ca_res_source"]=array();
				$system_cache["ca_res_source"]["alias"]=$account_alias;
				$system_cache["ca_res_source"]["name"]=$account_name;
				$system_cache["ca_res_source"]["bio"]=$account_bio;
				$system_cache["ca_res_source"]["avatar"]=$account_avatar;
				$system_cache["ca_res_source"]["header"]=$account_header;
				$system_cache_update=true;
			}
		}
	}else{
		$account_alias=$system_cache["ca_res_source"]["alias"];
		$account_name=$system_cache["ca_res_source"]["name"];
		$account_bio=$system_cache["ca_res_source"]["bio"];
		$account_avatar=$system_cache["ca_res_source"]["avatar"];
		$account_header=$system_cache["ca_res_source"]["header"];
	}
}

//##############################################################
//##############################################################-- Meta Details
//##############################################################

$meta_title="".$account_name."";
$meta_description="".makesafe($account_bio)."";
$meta_image="".$account_header."";
$meta_name="".$account_name."";
$meta_icon="".$account_avatar."";

//##############################################################
//##############################################################-- Follow Button
//##############################################################

//--Generate follow button
if ($system_config["heycafe_source_id"]!=false){
	if ($system_config["theme_show_follow"]==true){
		if ($system_config["heycafe_source_type"]=="account"){
			$content_extra.="<a href='https://hey.cafe/p/".$account_alias."'><div class='followme'>Follow on Hey.Café</div></a>";
		}else{
			$content_extra.="<a href='https://hey.cafe/c/".$account_alias."'><div class='followme'>Join on Hey.Café</div></a>";
		}
	}
}

//##############################################################
//##############################################################-- Body Content
//##############################################################

//--Load generators based on content area
if ($system_config["heycafe_account"]!=false){
	if ($system_page_type=="home"){ include("content_home.php"); }
	if ($system_page_type=="blog"){ include("content_blog.php"); }
	if ($system_page_type=="article"){ include("content_article.php"); }
}else{
	if ($system_page_type!="admin"){ header('Location: /admin'); }
}

if ($system_page_type=="admin"){ include("content_admin.php"); }
if ($system_page_type=="404"){ include("content_404.php"); }


//##############################################################
//##############################################################-- Header Content
//##############################################################

//--What to show
if ($system_config["heycafe_source_id"]!=false){
	$header_render=true;
	$header_full=true;
	$header_text="";
	
	if ($system_page_type=="admin"){
		$menu_render=false;
		$header_render=false;
	}
	
	$menucontent="";
	$menucontent.="<a href='/'>Homepage</a>";
	if ($system_config["theme_custom_homepage"]!=false){
		$menucontent.="<a href='/blog'>Blog</a>";
	}
	
	//--Load user provided menu
	if ($system_config["theme_custom_menu"]!=false){
		if (is_array($system_config["theme_custom_menu"])){
			foreach ($system_config["theme_custom_menu"] as $menu_item){
				$menucontent.="<a href='".$menu_item["link"]."'>".$menu_item["name"]."</a>";
			}
		}
	}
	
	if ($system_page_type=="home"){
		$header_text="<div class='bio'><div id='format_bio' style='margin-bottom:10px;'>".$account_bio."</div></div><action type='format_simple' element='format_bio'></action>";
	}
	
	if ($header_render==true){
		$content_header.="<div class='navbar'><a href='/'><img class='navbar_icon optimizer_img' alt='Website icon' source='".$account_avatar."' src='".$system_blank_image."'></a><div class='navbar_name'>".$account_name."</div><div class='navbar_menu'>".$menucontent."</div></div>";
		$content_header.="<div class='bannerbar'><div class='bannerbar_background optimizer_bg' style=\"background-image: url('".$system_blank_image."');\" source='".$meta_image."'><div class='inside'>".$header_text."</div></div></div>";
		//<div id='format_bio' style='margin-bottom:10px;'>".$account_bio."</div>
		//$content_header.="<action type='format_simple' element='format_bio'></action>";
	}
}

//##############################################################
//##############################################################-- Footer Content
//##############################################################

if ($system_config["theme_custom_footer"]==false){
	$content_footer.="<footer>Site contents are © ".$meta_name." ".date('Y').". Powered by <a href='https://gitlab.com/heycafe/simple-blog'>Hey.Café Blog</a>.</footer>";
}else{
	$content_footer.="<footer>".base64_decode($system_config["theme_custom_footer"])."</footer>";
}

//##############################################################
//##############################################################-- Formatting fixes from config
//##############################################################

if (str_contains($system_config["theme_colour"],"rgb")){
	$system_config["theme_colour"]=str_replace("rgb(", "", $system_config["theme_colour"]);
	$system_config["theme_colour"]=str_replace("rgba(", "", $system_config["theme_colour"]);
	$system_config["theme_colour"]=str_replace(")", "", $system_config["theme_colour"]);
	$system_config_update=true;
}

//##############################################################
//##############################################################-- Custom Content
//##############################################################

if ($system_config["theme_custom_css"]!=false){
	$content_head.="<style>".base64_decode($system_config["theme_custom_css"])."</style>";
}

//##############################################################
//##############################################################-- Save Changes to files
//##############################################################

if ($system_config_update==true){
	file_put_contents("./config.json", json_encode($system_config, JSON_PRETTY_PRINT));
}
if ($system_cache_update==true){
	file_put_contents("./cache.json", json_encode($system_cache, JSON_PRETTY_PRINT));
}

//##############################################################
//##############################################################-- Print out page
//##############################################################

//first update the metadata!
$system_theme=str_replace("{{meta_title}}", $meta_title, $system_theme);
$system_theme=str_replace("{{meta_description}}", $meta_description, $system_theme);
$system_theme=str_replace("{{meta_image}}", $meta_image, $system_theme);
$system_theme=str_replace("{{meta_name}}", $meta_name, $system_theme);
$system_theme=str_replace("{{meta_icon}}", $meta_icon, $system_theme);

$system_theme=str_replace("{{cache}}", $system_timestamp, $system_theme);

$system_theme=str_replace("{{content_head}}", $content_head, $system_theme);
$system_theme=str_replace("{{content_header}}", $content_header, $system_theme);
$system_theme=str_replace("{{content_body}}", $content_body, $system_theme);
$system_theme=str_replace("{{content_footer}}", $content_footer, $system_theme);
$system_theme=str_replace("{{content_extra}}", $content_extra, $system_theme);
$system_theme=str_replace("{{content_script}}", $content_script, $system_theme);

$system_theme=str_replace("{{theme_colour}}", $system_config["theme_colour"], $system_theme);
$system_theme=str_replace("{{theme_colour_contrast}}", readableColour($system_config["theme_colour"]), $system_theme);

echo $system_theme;